#FROM orleon/lamp:latest
FROM mattrayner/lamp:latest-1804

COPY www/ /app/
COPY create_mysql_users.sh /
COPY mysql-setup.sh /
COPY run.sh /
RUN chmod +x /*.sh

RUN sed -i 's/max_file_uploads.*/max_file_uploads = 1000/' /etc/php/7.4/cli/php.ini
RUN sed -i 's/max_file_uploads.*/max_file_uploads = 1000/' /etc/php/7.4/apache2/php.ini
RUN sed -i 's/upload_max_filesize.*/upload_max_filesize = 1000M/' /etc/php/7.4/cli/php.ini
RUN sed -i 's/upload_max_filesize.*/upload_max_filesize = 1000M/' /etc/php/7.4/apache2/php.ini
RUN sed -i 's/post_max_size.*/post_max_size = 1000M/' /etc/php/7.4/cli/php.ini
RUN sed -i 's/post_max_size.*/post_max_size = 1000M/' /etc/php/7.4/apache2/php.ini

EXPOSE 80 3306
CMD ["/run.sh"]

